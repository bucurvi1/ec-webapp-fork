#
# Quick and dirty script to do a DNS lookup. This is useful when a tools like dig/host/nslookup
# won't work if proxy settings are in place (since dig/host/nslookup are not proxy aware).
#
# Currently only looks for A records, but clearly this can be expanded in various ways.
#
# This is intended for command line use. The dns-api.org service does not support high volumes

import sys
import requests
import json

def displayResult(response, displayType='IP'):
    # Just print the IP address - useful to use in this context: curl http://`python dnslookup.py somehost`
    if displayType == 'IP':
        for record in response:
            if record['type'] == 'A':
                print record['value']
    else:
        print json.dumps(response, indent=4)

########
# Main #
########
if len(sys.argv) != 2:
    print 'usage: %s <hostname>' % sys.argv[0]
    sys.exit(1)

host = sys.argv[1]
recordType = 'A'

url = 'https://dns-api.org/%s/%s' % (recordType, host)
r = requests.get(url)

if r.status_code == requests.codes.ok:
    displayResult(r.json(), 'x')
else:
    print 'Error looking up DNS name'
    r.raise_for_status()
