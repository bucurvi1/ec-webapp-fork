/*
 * Create a generic web application (or web services) environment with an
 * Elastic Load Balancer in front of an Autoscale Group.
 *
 * As currently configured, runs Nginx on each EC2 instance running RHEL 6.5.
 * The ELB listens on port 80, but the EC2s listen on 8080. SSSL will be added.
 *
 * Also creates an RDS/Aurora cluster
 *
 * See module details for actual configuration specifics
 */

############
# Web tier #
############
# Include webapp tier (ELB plus autoscaling group)
module "webtier" {
    source = "./webtier"

    webappname = "${var.appname}"
    nibr_ids = "${var.nibr_ids}"
    nibr_technicalowner = "${var.nibr_technicalowner}"
    nibr_managedby = "${var.nibr_managedby}"
    nibr_environment = "${var.nibr_environment}"
    region = "${var.region}"
    subnets = "${var.subnets}"
}

# Collect the output results from the web tier and display them
output "webtier_elb_URL"       { value = "${module.webtier.elb_URL}" }
output "webtier_elb_CNAME_URL" { value = "${module.webtier.elb_CNAME_URL}" }


###########
# DB tier #
###########
variable "rds_password"        { description = "Enter a password for the RDS Aurora database" }

# Include RDS/Aurora cluster
module "dbtier" {
    source = "./dbtier"

    dbappname = "${var.appname}"
    nibr_ids = "${var.nibr_ids}"
    nibr_technicalowner = "${var.nibr_technicalowner}"
    nibr_managedby = "${var.nibr_managedby}"
    nibr_environment = "${var.nibr_environment}"
    subnets = "${var.subnets}"
    zones = "${var.zones}"
    rds_password = "${var.rds_password}"
}

# Collect the output results from the DB tier and display them
output "dbtier_cluster_endpoint" { value = "${module.dbtier.rds_endpoint}" }
output "dbtier_cluster_port"     { value = "${module.dbtier.rds_port}" }
output "dbtier_cluster_username" { value = "${module.dbtier.rds_username}" }
output "dbtier_cluster_password" { value = "${module.dbtier.rds_password}" }
