# Old file to show some examples of Terraform syntax and patterns for working with variables

variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.

Example: ~/.ssh/terraform.pub
DESCRIPTION
}

variable "key_name" {
  description = "Desired name of AWS key pair"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default = "us-east-1"
}

# TODO: These are Amazon Linux AMIs. Change to NIBR AMIs in the future
# Amazon Linux AMI 2016.03.3 (HVM), SSD Volume Type
variable "aws_amis" {
  type = "map" # Type is inferred if default provided and can therefore by omitted
  default = {
    us-east-1 = "ami-6869aa05"
    eu-central-1 = "ami-ea26ce85"
  }
}

variable "access_key" {} # Defines variable to be filled in via UI, CLI switch, of terraform.tfvars
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}
