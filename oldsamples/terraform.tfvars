# An example of how to use a file to set values to already defined variables

# The name of this file is a default name and as such will result in
# var.access_key and var.secret_key being assigned
access_key = "accesskey-goes-here"
secret_key = "secret-access-key-goes-here"
