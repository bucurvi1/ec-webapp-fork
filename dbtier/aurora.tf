/*
 Configure an AWS Aurora RDS cluster
 
 Takes about 6 minutes to create and about 5 to destroy

 TODO I don't have experience with RDS and while this all works, there are finer details that 
 need to be reviewed and tuned.

 This form does not work since DNS is not working!
 mysql -u king -h kenbp-aurora-cluster.cluster-cg9jtvl6wqvw.us-east-1.rds.amazonaws.com -p mydb
 As a workaround, use mxtoolbox.com (or anything else) to lookup IP and then:

 This is a good command line lookup means:
 curl https://dns-api.org/A/some-cluster-endpoint-name-prefix.us-east-1.rds.amazonaws.com \
   | json_pp
 curl --progress-bar https://dns-api.org/A/some-cluster-endoint-name-prefix-us-east-1.rds.amazonaws.com \
   | json_pp | grep '10.18' | cut -d':' -f2

 I created a python utility that gets the DNS name in a way that it can be used inline.
 You need to first install 'request' using: pip install requests
 You can then use inline as follows:
     mysql -u king -p -h `python dnslookgup.py somehostname` mydb

 If you know the IP address, this format will work:
     mysql -u king -p -h 10.188.250.45 mydb
 Enter password when prompted.
 Run an example query to make sure it's alive.
     > select current_user();
*/

resource "aws_db_subnet_group" "default" {
    name = "${var.dbappname}-db-subnet-group"
    subnet_ids = "${var.subnets}"
    tags {
        "nibr:ids"            = "${var.nibr_ids}"
        "nibr:technicalowner" = "${var.nibr_technicalowner}"
        "nibr:managedby"      = "${var.nibr_managedby}"
        "nibr:environment"    = "${var.nibr_environment}"
    }
}

resource "aws_rds_cluster_instance" "rds_cluster_instances" {
    count                = "${var.rds_cluster_size}"
    identifier           = "${var.dbappname}-aurora-cluster-${count.index}"
    cluster_identifier   = "${aws_rds_cluster.rds_cluster.id}"
    instance_class       = "${var.rds_instance_class}"
    db_subnet_group_name = "${aws_db_subnet_group.default.name}"
    storage_encrypted    = true
    # TODO monitoring_role_arn  = "TODO"
    monitoring_interval = 0 # Valid Values: 0, 1, 5, 10, 15, 30, 60. 0 disables. # TODO Set to non 0?
    tags {
        "nibr:ids"            = "${var.nibr_ids}"
        "nibr:technicalowner" = "${var.nibr_technicalowner}"
        "nibr:managedby"      = "${var.nibr_managedby}"
        "nibr:environment"    = "${var.nibr_environment}"
    }
}

resource "aws_rds_cluster" "rds_cluster" {
    cluster_identifier     = "${var.dbappname}-aurora-cluster"
    availability_zones     = "${var.zones}"
    vpc_security_group_ids = "${var.dbtier_sgs}"
    db_subnet_group_name   = "${aws_db_subnet_group.default.name}"
    database_name          = "${var.rds_database_name}"
    master_username        = "${var.rds_master_username}"
    master_password        = "${var.rds_password}"
    storage_encrypted      = true
}

output "rds_endpoint" { value = "${aws_rds_cluster.rds_cluster.endpoint}" }
output "rds_port"     { value = "${aws_rds_cluster.rds_cluster.port}" }
output "rds_username" { value = "${aws_rds_cluster.rds_cluster.master_username}" }
output "rds_password" { value = "${aws_rds_cluster.rds_cluster.master_password}" }
