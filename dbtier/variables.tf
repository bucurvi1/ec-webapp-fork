############################################################
# Review and change these values for your particular needs
############################################################

##########
# Common #
##########

# Most common values all have defaults, but the calling module should explicitly set each

variable "dbappname" { default = "dbtier-not-set" }

# Tags values
variable "nibr_ids"            { default = "dbappname=dbtier-appname-not-set" }
variable "nibr_technicalowner" { default = "not-set" }
variable "nibr_managedby"      { default = "terraform=not-set" }
variable "nibr_environment"    { default = "dev" }

# Where to deploy
# Subnet list corresponds to zone list which corresponds to this CIDR list
variable "subnets" {
    type = "list"
    description = <<DESCRIPTION
You cannot enter lists via the UI.
Use -var or -var-file instead.
To use -var-file, edit local.tfvars and add to the command line: -var-file="local.tfvars"
subnets should be a list of subnet IDs.
DESCRIPTION
}
variable "zones" {
    type = "list"
    description = <<DESCRIPTION
You cannot enter lists via the UI.
Use -var or -var-file instead.
To use -var-file, edit local.tfvars and add to the command line: -var-file="local.tfvars"
zones should be a list of availability zones.
DESCRIPTION
}


###########
# DB Tier #
###########

# RDS/Aurora configuration parameters
variable "rds_cluster_size"    { default = 2 }
variable "rds_instance_class"  { default = "db.r3.large" }
variable "rds_database_name"   { default = "mydb" } # Max of 8 alphanumeric chars
variable "rds_master_username" { default = "king" }
variable "rds_password"        { description = "Enter a password for the RDS Aurora database" }
variable "dbtier_sgs"          { default = ["sg-7074fd0b", "sg-c074fdbb"] }
                                # default SG. Add AllVPC. TODO This needs review
