/*
Configure Terraform provider
Get credentials from environment variables by first running:
    source aws.creds

aws.creds should look like:
    export AWS_ACCESS_KEY_ID=key_ID_here
    export AWS_SECRET_ACCESS_KEY=key_here
*/
provider "aws" {
    region = "${var.region}"

    /*
    The access_key and secret_key credentials are being read from the environment
    variables listed above. This is probably the method that is more compatible for future
    automation means that we'll use for launching Terraform.
    */

    /*
    The following alternative approach of reading directly from the ./aws/credentials is not working for me,
    but others have got it to work. In theory, I should be able to just set the profile and if
    my credentials (as created by 'aws configure') are in the default location of ~/.aws/credentials
    this should work.

    profile                  = "TL-NX-SLT"

    Even if I explicitly set the shared_credentials_file it still does not work. E.g.,:
    Both of the following forms fail:
    shared_credentials_file  = "/Users/robbike1/.aws/credentials"
    shared_credentials_file  = "~/.aws/credentials"
    */
}
