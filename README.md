# Webapp Blueprint

**Version 0.2**


This webapp blueprint is Terraform configuration (and some supporting scripts) that will deploy the resources
and configuration to create a canonical web application that is compatible with NIBR's Turbot-managed AWS environment.
This will work in either a Tech Lab or an A&S type of account.

## Specifications
The resulting infrastructure that is created by this blueprint has the following features:

* Https (SSL) endpoint for browser access (certificate is created and applied)
* Automatic creation of DNS CNAME entry
* Load balancer (ELB) to distribute load across 2 availability zones within a single region
* Autoscaling group of EC2 servers to provide automatic scaling as well as self-healing for failed nodes
* Security groups are automatically applied to properly encapsulate and protect resources
* Nginx web server returns a simple static HTML page as a means to demonstrate that the system is operating (presumably this is replaced by your code)
* Managed database cluster (RDS) running the Aurora database

## Design description
The resulting configuration listens to https port 443 on a load balancer and directs that traffic to an
autoscaling group of EC2 instances. Each is running Nginx and listens on http port 8080. The default web app
will access an RDS/Aurora database cluster.

In addition to creating the AWS resources (ELB, autoscaling group of EC2s, and RDS cluster), the Terraform execution
will automatically create/update an appropriate DNS entry in NIBR's Infoblox DNS server. Also included is a script
to run to automatically generate an SSL certificate that is automatically attached to the ELB.

For now, the configuration assumes that the Turbot options have been manually set. In the future, we might make
this programmatic or at least programmatically check. However, I'm not sure there is value in that level of 
automation, so this is left as a manual set up step for now.

The EC2 instances will automatically find the most recent NIBR Centos 6.5 AMI.

Primary Turbot requirements are:

* Enable EC2
* Enable S3
* Enable ACM
* Enable RDS
* Enable Aurora

The web interface is available to all Novartis, but the DB is protected so no direct DB access is possible.

* Connection to DB (from app tier) is not yet implemented (only a static web page is returned, index.html). You can ssh to one of the EC2's and run the mysql client to test access to the RDS cluster.

![webapp_blueprint.png](https://bitbucket.org/repo/dMjapb/images/2052660588-webapp_blueprint.png)

# Usage
To use this blueprint, clone the repo, review and edit all of the values in the variables.tf files, and run the
Terraform commands as defined below. This blueprint will continue to evolve, but the idea is that you copy it and use
it as a starting point to customize for your unique needs. For example, if you don't need an RDS database, comment
out or delete that module. Or, if you don't really need autoscaling and only need one server, but do want the
self-healing attributes, then set the minimum and maximum autoscaling group size to 1.

```
source aws.creds              # To set AWS API credentials (or use any other appropriate method)
source webtier/infoblox.creds # To set Infoblox API credentials
Review and edit: ./variables.cf, webtier/variables.tf, dbtier/variables.tf
cd webtier
createcert.sh <hostname>      # Run this once for a given hostname to create an SSL cert
cd ..
# Wait for cert to be issued (could be an hour or a couple of days) and then proceed.
# Alternatively, in webtier/webapp.tf set the ELB listener to be http/80 and then change to https/443
# once cert is approved and just run terraform apply to change the listener
terraform get                 # Required to load sub modules
terraform plan                # Review to see what will be created when applied for real
terraform apply               # Create the resources. Takes about 6 minutes.
# Or to override the default region of us-east=1, use the following form:
terraform apply -var 'region=eu-central-1' 

# Testing
## Front-end (web tier)
The above step will print webtier_elb_URL and webtier_elb_CNAME_URL. To test, you can browse to one or both of these URLs.
The test that really matters is the webtier_elb_CNAME_URL.
The webtier_elb_URL may be useful for debugging though since it doesn't require that the DNS change worked.

Use http or https depending on how you configured the ELB front end listener. 

The apply step does attempt to run a curl command to self test. You can also execute a curl command such as:
curl -k <webtier_elb_URL or webtier_elb_CNAME_URL that is printed from the above step>  # A test that should return the Nginx default page.

Note that I've seen variable DNS propogation issues where the self-test worked, but a browser test didn't (and then
it did). I'm not quite sure what was happening, so for now, I'll just give you the heads up.

## Back-end (database tier)
Since at present this blueprint is focused on creating infrastructure the application is as thin as possible to show
that things are working. That is, the front-end only returns a static page and does not attempt to reach back to the DB.
Therefore, to test the DB tier, ssh to one of the instances in the autoscale group and run:
curl --progress-bar https://dns-api.org/A/some-cluster-endoint-name-prefix-us-east-1.rds.amazonaws.com \
   | json_pp | grep '10.18' | cut -d':' -f2
  # This gets the IP address of RDS cluster. I used to need this due to DNS issues that we haven't resolved.
  # However, I think that this is fixed now, but I'll leave this doc here until I re-test.
mysql -u king -p -h <IP address from above or cluster DNS name if that is working> mydb
  # At the msql prompt type:
> select current_user();

# Other useful commands
terraform plan -destroy       # To see what will be destroyed if terraform destroy is run
terraform plan -var-file local.tfvars # Good for running in each module's directory
terraform show                # To show the current state of deployed resources
terraform destroy             # To remove all resources previously created by this configuration
terraform graph | dot -Tpng > graph.png # To create a PNG file that represents the dependency graph
# An example of how to apply just a single resources instead of the default all:
terraform plan -target=aws_launch_configuration.webtier_launch_conf -out tempplan
terraform apply tempplan
```

# TODO
Since Turbot updates bucket separate from Terraform, I needed to run plan and then hand edit the
plan to make it match the current version so.
In particular I needed to update policy and logging on bucket.
This is only an issue if I want to ever run apply more than once. The first time will work since
Turbot will not have had a chance to add its stuff then; the problem is only with updates.

* DNS resolution of RDS cluster endpoint from web tier is not working - this is a Tech Lab firewall issue
* Should I change local shell scripts to Python for portability?
* Update AMI handling and SG handling once we fix our AMI and SG publishing to Turbot environments
* Fix bucket logging
* Set up SG generic names
* Change how aws.creds is used. Put in aws.tfvars and use with -var-file="aws.tfvars" or put in terraform.tfvars?
  Or is environment var option better?  For ideas see: https://www.terraform.io/intro/getting-started/variables.html
* Back out proxy changes to yum.conf now that hairpin is working?

* Review TODOs in code...
* Cluster policy (did I mean RDS or something else?) appears to not have encryption required by policy. This seems like a mistake.
* Update this Cyber page: http://web.global.nibr.novartis.net/apps/confluence/display/Cyber/Requesting+an+Amazon+Certificate+Manager+%28ACM%29+TLS+certificate
* Update this page: http://web.global.nibr.novartis.net/apps/confluence/pages/viewpage.action?pageId=75827954

# Resources and prerequisites
Install the following:

* AWS CLI - http://aws.amazon.com/cli
  * Get API keys and save to aws.creds file
* Terraform - https://www.terraform.io
* Graphviz - http://www.graphviz.org (required only if you want to use 'dot' command above)
* Ask DevOps to grant you access to the Infoblox API (this is not a productive API and they may not be able to accomodate)
* Git - Git is required if a Terraform configuration references a GitHub or BitBucket module source
