#
# Create a CNAME record in the NIBR Infoblox DNS server
#
# TODO: For this to be useful with Terraform, create needs to automatically do an update if it already exists
# and delete needs to ignore the target.
# I also need an easy query interface
#
# This script uses an interface to Infoblox that is not yet offically in production.
# Therefore, this is more of a POC for the moment. You can always just ask DevOps to create
# the record for you, but using a script like this (and called from automation) is the direction
# that we want to go.
#
# For now this just creates/deletes CNAME records.
# A future iteration should support updates (PUT)
# A future iteration could support A and PTR records
#
# Be sure to set the credential environment variables
# To do this they can be put in a file containing the following lines and then source the file.
# E.g., source infoblox.creds
# export INFOBLOX_API_USER=<username>
# export INFOBLOX_API_PASSWORD=<password>
# The best was to do this is to:
#     source infoblox.creds
# This method avoids leaving your password in any files or command history
# I suppose I could just give an interaction option to this script. I'll do that in a separate update.
#
# Infoblox service information:
#   Swagger: http://infoblox.cloud.nibr.novartis.net/
#   Wiki documentation: http://web.global.nibr.novartis.net/apps/confluence/display/DO/Infoblox  

import sys
import os
import requests
import json
import argparse

def validateHostname(host):
    '''
    Do some basic validation. Since the standards may change, this might need to be updated
    and does not try to be overly complete.
    Format:
    <userDefinePartWithNoDots>.<zone>.nibr.novartis.net
    where zone is one of:
    cloud
    tst
    prd

    In the future, dev, test, prod, cluster, sandbox, external may be supported
    Use: dig soa <zone>.nibr.novartis.net
    to see what if Infoblox or QIP is serving the zone
    '''
    if not host.endswith('.nibr.novartis.net'):
        print 'ERROR: hostname (%s) does not end with .nibr.novartis.net' % host
        return False

    parts = host.split('.')
    if len(parts) != 5:
        print 'ERROR: Invalid number of dotted elements (expected 5) in hostname %s' % host
        return False

    if not parts[1] in ('cloud', 'tst', 'prd'):
        print 'ERROR: specified zone (%s) is not supported by Infoblox' % parts[1]
        return False

    return True

def createCname(user, password, host, target, ttl):
    '''
    Create a CNAME entry in Infoblox
    Return True on success. False on validation error. Exit with exception on API call error.

    No need to check if available, since it will fail if there is an existing record (of any type).
    '''
    if not validateHostname(host):
        return False

    url = 'http://infoblox.cloud.nibr.novartis.net/v1/cname'
    headers={
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
    data = 'name=%s&canonical=%s&issuer=%s&ttl=%d' % (host, target, user, int(ttl))
    r = requests.post(
            url, 
            auth=(user, password),
            headers=headers,
            data=data
            )

    if r.status_code >= 200 and r.status_code < 300:
        print json.dumps(r.json(), indent=4)
        print 'CNAME record successfully created in Infoblox'
        print '%s. %d IN CNAME %s.' % (host, int(ttl), target)
        return True
    else:
        print 'Status code:', r.status_code
        print 'Error creating CNAME record'
        r.raise_for_status()
        return False

def deleteCname(user, password, host):
    '''
    Delete a CNAME entry in Infoblox
    Return True on success. False on validation error. Exit with exception on API call error.

    No need to check if it exists since it can't hurt to delete something that doesn't exist.
    '''
    if not validateHostname(host): # Not really necessary, but may help to make errors more obvious
        return False

    url = 'http://infoblox.cloud.nibr.novartis.net/v1/cname?name=%s&issuer=%s' % (host, user)
    headers={
        'Accept': 'application/json'
    }
    r = requests.delete(
            url, 
            auth=(user, password),
            headers=headers
            )

    if r.status_code >= 200 and r.status_code < 300:
        body = r.json()
        print json.dumps(body, indent=4)
        if len(body['deleted_cname']) == 0:
            print 'WARNING: There was nothing to delete'
        else:
            print 'CNAME record successfully deleted from Infoblox'
        return True
    else:
        print 'Status code:', r.status_code
        print 'Error deleting CNAME record'
        r.raise_for_status()
        return False

def getCname(user, password, host):
    '''
    GET a CNAME entry in Infoblox
    Return True on success. False on validation error. Exit with exception on API call error.
    '''
    if not validateHostname(host): # Not really necessary, but may help to make errors more obvious
        return False

    url = 'http://infoblox.cloud.nibr.novartis.net/v1/cname?name=%s' % (host)
    headers={
        'Accept': 'application/json'
    }
    r = requests.get(
            url, 
            auth=(user, password),
            headers=headers
            )

    if r.status_code >= 200 and r.status_code < 300:
        body = r.json()
        if len(body) == 0:
            print 'WARNING: The requested CNAME not found'
        else:
            print json.dumps(body, indent=4)
        return True
    else:
        print 'Status code:', r.status_code
        print 'Error getting CNAME record'
        r.raise_for_status()
        return False

def usageError(msg):
    print 'Usage error: ', msg
    print 'Try: python dnsupdate --help'
    sys.exit(1)

########
# Main #
########
parser = argparse.ArgumentParser()
parser.add_argument('command', help='One of: create | delete | get')
parser.add_argument('-H', '--host', help='Hostname for the DNS record')
parser.add_argument('-t', '--target', help='Target for the DNS record')
parser.add_argument('-u', '--user',
        help='User ID for Infoblox login. Can also put in environment variable INFOBLOX_API_USER',
        default=os.environ.get('INFOBLOX_API_USER'))
parser.add_argument('-p', '--password',
        help='Password for Infoblox login. Can also put in environment variable INFOBLOX_API_PASSWORD',
        default=os.environ.get('INFOBLOX_API_PASSWORD')
        )
parser.add_argument('-T', '--ttl', help='Time to live for DNS record', type=int, default=900)
args = parser.parse_args()

if args.host is None:
    usageError('Missing hostname')

if args.command == 'create':
    if args.target is None:
        usageError('Missing target')
    else:
        createCname(args.user, args.password, args.host, args.target, args.ttl)
elif args.command == 'delete':
    deleteCname(args.user, args.password, args.host)
elif args.command == 'get':
    getCname(args.user, args.password, args.host)
else:
    usageError('Unknown command: ' + command)

