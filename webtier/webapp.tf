/*
Create an EC2 autoscaling group with an ELB in front.
*/

#######################################################################
# LAUNCH CONFIGURATION
# Define the launch configuration for the app servers
#######################################################################
resource "aws_launch_configuration" "webtier_launch_conf" {
    lifecycle { create_before_destroy = true }
    name_prefix     = "${var.webappname}-webtier-launch_conf-"
    # image_id        = "${lookup(var.aws_amis, var.region)}"
    image_id        = "${data.aws_ami.currentAMI.id}"
    instance_type   = "${var.webtier_instance_type}"
    key_name        = "${var.ssh_key_name}"
    security_groups = "${var.webtier_ec2_sgs}"
    user_data       = "${file("${path.module}/boot.sh")}"
}


#######################################################################
# AUTOSCALE GROUP
# Create the app server tier autoscale group (launches instances)
#######################################################################
resource "aws_autoscaling_group" "webtier_asg" {
    lifecycle { create_before_destroy = true }
    name                 = "${var.webappname}-webtier-asg"
    min_size             = "${var.webtier_min_size}"
    max_size             = "${var.webtier_max_size}"
    desired_capacity     = "${var.webtier_desired_capacity}"
    min_elb_capacity     = "${var.webtier_min_elb_capacity}"
    launch_configuration = "${aws_launch_configuration.webtier_launch_conf.name}"
    vpc_zone_identifier  = "${var.subnets}"
    load_balancers       = ["${aws_elb.webtier_elb.id}"]
    force_delete         = true
    health_check_grace_period = 1800 # !!! Make sure that this is long enough for boot.sh to finish.   !!!
                                     # !!! And account for long yum updates if that's part of process. !!!
                                     # RHEL 6.5 on a t2.micro with yum install nginx takes just under 7 minutes.
    health_check_type    = "ELB"

    tag { key = "nibr:ids"            value = "${var.nibr_ids}"            propagate_at_launch = true }
    tag { key = "nibr:technicalowner" value = "${var.nibr_technicalowner}" propagate_at_launch = true }
    tag { key = "nibr:managedby"      value = "${var.nibr_managedby}"      propagate_at_launch = true }
    tag { key = "nibr:environment"    value = "${var.nibr_environment}"    propagate_at_launch = true }
}

#######################################################################
# LOAD BALANCER
# Create load balancer to sit in front of app server tier
#
# Before running this, be sure to run createcert.sh once (for each unique
# domain name). createcert.sh creates cert.tf which sets the variable elbCertARN.
# 
# A null_resource is created as a post elb creation task. This
# null_resource is used to create/update the DNS entry.
#######################################################################
resource "aws_elb" "webtier_elb" {
    name = "${var.webappname}-webtier-elb"
    subnets  = "${var.subnets}"
    security_groups = "${var.webtier_elb_sgs}"
    internal = true

    /* TODO Disabled for now. This is wasting too much time trying to get to work. Having permission trouble
    * such that apply can't access bucket
    access_logs {
        bucket = "${aws_s3_bucket.log_bucket.bucket}"
        bucket_prefix = "webtier"
        interval = 60
    }
    *****/

    listener {
        lb_port = "${var.lb_port}"
        lb_protocol = "${var.lb_protocol}"
        instance_port = 8080
        instance_protocol = "http"
        ssl_certificate_id = "${var.elbCertARN}" # This needs to be commented out if protocol is http (and not https)
    }

    health_check {
        healthy_threshold = 2
        unhealthy_threshold = 3
        timeout = 3
        target = "HTTP:8080/"
        interval = 30
    }

    cross_zone_load_balancing = true
    idle_timeout = 400
    connection_draining = true
    connection_draining_timeout = 400

    tags {
        "nibr:ids"            = "${var.nibr_ids}"
        "nibr:technicalowner" = "${var.nibr_technicalowner}"
        "nibr:managedby"      = "${var.nibr_managedby}"
        "nibr:environment"    = "${var.nibr_environment}"
    }
}

resource "null_resource" "dns" {
    # Update the DNS record
    # Delete the old DNS record (if one exists) and create a new one (since dns_name changes)
    provisioner "local-exec" {
        command = "python ${path.module}/dnsupdate.py delete -H ${var.hostname}"
    }
    provisioner "local-exec" {
        command = "python ${path.module}/dnsupdate.py create -H ${var.hostname} -t ${aws_elb.webtier_elb.dns_name} -T ${var.DNS_TTL}"
    }
    provisioner "local-exec" {
        command = "curl -k ${var.lb_protocol}://${aws_elb.webtier_elb.dns_name}"
    }
    # TODO: This may not work depending on DNS propogation time or TTL on previous entry
    provisioner "local-exec" {
        command = "curl -k ${var.lb_protocol}://${var.hostname}"
    }

    depends_on = ["aws_autoscaling_group.webtier_asg"]
}

#######################################################################
# RETURN RESULTS
# Returns some useful output values (so that parent module can display)
#######################################################################
output "elb_dns_name"  { value = "${aws_elb.webtier_elb.dns_name}" }
output "elb_URL"       { value = "${var.lb_protocol}://${aws_elb.webtier_elb.dns_name}" }
output "elb_CNAME"     { value = "${var.hostname}" }
output "elb_CNAME_URL" { value = "${var.lb_protocol}://${var.hostname}" }
