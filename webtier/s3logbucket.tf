# Create a bucket for logs and such

# Come back to this later and engineer this to fit into the module structure

/*************** Getting ACL error that I don't want to fight with right now
resource "aws_s3_bucket" "log_bucket" {
    bucket = "ken-webapp"

    lifecycle_rule {
        id = "webtier_logs"
        prefix = "webtier/"
        enabled = true

        transition {
            days = 30
            storage_class = "STANDARD_IA"
        }
        transition {
            days = 60
            storage_class = "GLACIER"
        }
        expiration {
            days = 90
        }
    }

    tags {
        Name = "Ken test bucket creation"
    }

    # Added manually to sync with automatic Turbot updates to bucket
    logging {
        target_bucket = "taa-usea1-8aqn4unxhym7"
        target_prefix = "AWSLogs/935841621817/S3/ken-webapp/"
    }

    # Added manually to sync with automatic Turbot updates to bucket
    policy = "{\"Statement\":[{\"Action\":\"s3:PutObject\",\"Condition\":{\"StringNotEquals\":{\"s3:x-amz-server-side-encryption\":\"AES256\"}},\"Effect\":\"Deny\",\"Principal\":\"*\",\"Resource\":[\"arn:aws:s3:::ken-webapp\",\"arn:aws:s3:::ken-webapp/*\"],\"Sid\":\"MustBeEncryptedAtRest\"},{\"Action\":\"s3:*\",\"Condition\":{\"Bool\":{\"aws:SecureTransport\":\"false\"}},\"Effect\":\"Deny\",\"Principal\":\"*\",\"Resource\":[\"arn:aws:s3:::ken-webapp\",\"arn:aws:s3:::ken-webapp/*\"],\"Sid\":\"MustBeEncryptedInTransit\"}],\"Version\":\"2012-10-17\"}"
}
**********************/
# TODO I tried to add this to allow ELB to write to bucket, but it didn't help
#    { \"Sid\": \"AllowELBtoWriteLogs\", \"Effect\": \"Allow\", \"Principal\": { \"AWS\": \"arn:aws:iam::127311923021:root\" }, \"Action\": \"s3:PutObject\", \"Resource\": \"arn:aws:s3:::ken-webapp/webtier/935841621817/*\" }

