#!/bin/bash
# This script is structured to be included in the body of the user_data
# field on an AWS instance.
#
# This script will be run as root upon image boot (and only upon the first boot).
#
# This version is hard coded for RHEL 6 running in us-east-1.
#
# See /var/log/cloud-init-output.log on the target instance for output.
#
# Currently this takes about 6.5 minutes to run on a t2.micro.
#
# There is currently no error checking (there should be for a real deployment)

# Turn off DNS checking in sshd since we don't have reverse DNS working and
# this causes long login delays while it tries to do the reverse DNS lookup.
sed -i -e 's/^UseDNS/#UseDNS/' /etc/ssh/sshd_config
echo Commented out old UseDNS lines in /etc/ssh/sshd_config

sed -i -e '$ a\
UseDNS no' /etc/ssh/sshd_config
echo Added \'UseDNS no\' line to /etc/ssh/sshd_config

service sshd reload
echo Reloaded sshd config

# TODO: Hard coded for US for the moment. Add EU proxy if in EU
# This must be an IP in our current environment since use are
# relying on the proxy to be the name server. Therefore, there's a Catch-22
# if we try to specify the proxy with a FQDN.
# US (usca-proxy01.na.novartis.net:2011): 160.62.237.221:2011
# EU (chbs-dmz-proxy01.nibr.novartis.net:2011):  10.168.240.221:2011
sed -i -e '$ a\
proxy=http://160.62.237.221:2011' /etc/yum.conf
# EU: proxy=http://10.168.240.221:2011' /etc/yum.conf
echo Added US proxy line to yum.conf

# This is for RHEL 6 only
cat <<EOF > /etc/yum.repos.d/nginx.repo
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/rhel/6/\$basearch/
gpgcheck=0
enabled=1
EOF
echo Created /etc/yum.repos.d/nginx.repo

# This could take a very long time depending on the image
# echo About to yum update
# yum update -y
# echo Yum update complete
echo Yum update SKIPPED

echo About to yum install mysql
yum install -y mysql
echo Mysql install complete

echo About to yum install nginx
yum install -y nginx
echo Nginx install complete

sed -i -e '/80;/s//8080;/' /etc/nginx/conf.d/default.conf
echo Changed nginx to listen to 8080 instead of 80 in /etc/nginx/conf.d/default.conf

# For RHEL 7 use: sudo systemctl start nginx.service
service nginx start
echo Nginx service started

# For RHEL 7 use: sudo systemctl enable nginx.service
# For RHEL 6, the following may not be necessary, but do anyway
chkconfig nginx on
echo Nginx configured to run on bootup

echo Script complete

