# Find the most recent standard NIBR CentOS 6 AMI

data "aws_ami" "currentAMI" {
    most_recent = true
    filter {
        name = "name"
        values = ["nibr baseos centos 6 x86_64 20*"]
    }
    owners = ["250523745930"] # Account ID for "Prod - AMI Management" account
}

output "found_ami_id"   { value = "${data.aws_ami.currentAMI.id}" }
output "found_ami_desc" { value = "${data.aws_ami.currentAMI.description}" }
output "found_ami_name" { value = "${data.aws_ami.currentAMI.name}" }
output "found_ami_tags" { value = "${data.aws_ami.currentAMI.tags}" } # TODO: For some reason, shows nothing



