############################################################
# Review and change these values for your particular needs
############################################################

##########
# Common #
##########

# Most common values all have defaults, but the calling module should explicitly set each

variable "webappname" { default = "webtier-not-set" }

# Tags values
variable "nibr_ids"            { default = "webappname=webtier-appname-not-set" }
variable "nibr_technicalowner" { default = "not-set" }
variable "nibr_managedby"      { default = "terraform=not-set" }
variable "nibr_environment"    { default = "dev" }

# Where to deploy
# Subnet list corresponds to zone list which corresponds to this CIDR list
variable "region"  { default = "us-east-1" }  # Use eu-central-1 for the Frankfurt region
variable "subnets" {
    type = "list"
    description = <<DESCRIPTION
You cannot enter lists via the UI.
Use -var or -var-file instead.
To use -var-file, edit local.tfvars and add to the command line: -var-file="local.tfvars"
subnets should be a list of subnet IDs.
DESCRIPTION
}

# Used for DNS server configuration
variable "DNS_TTL"  { default = "600" }
variable "hostname" { default = "webappbp-app.cloud.nibr.novartis.net" }
# The .cloud. should really be .dev., but we haven't yet moved .dev. to Infoblox.
# Therefore, use .cloud.nibr.novartis.net for dev hostnames for now.
# variable "hostname" { default = "NOTSET-app.cloud.nibr.novartis.net " }

# ELB protocol (SSL or not)
# If set to https, then in webtier.tf comment out the line: ssl_certificate_id = "${var.elbCertARN}" 
variable "lb_protocol"  { default = "https" }    # Or http or https (may need to comment out cert line in webtier.tf if http
variable "lb_port"      { default = 443 }        # Or 80 or 443


############
# Web tier #
############

# Autoscaling group scale parameters
variable "webtier_min_size"         { default = 1 }
variable "webtier_max_size"         { default = 3 }
variable "webtier_desired_capacity" { default = 2 }
variable "webtier_min_elb_capacity" { default = 1 }  # Used for startup only.
                                                     # How many nodes required for Terraform to say it's okay.

# Instance information
variable "webtier_instance_type" { default = "t2.micro" }
/* No longer used. Delete this
# TODO: These are community RHEL 6.5 AMIs. Change to NIBR AMIs in the future
variable "aws_amis" {
    default = {
        us-east-1       = "ami-00a11e68" # RHEL 6.5 - not sure if this is best RHEL 6 to use or not
        eu-central-1    = "ami-008ebf1d" # RHEL 6.5 - not sure if this is best RHEL 6 to use or not
    }
}
*/
variable "ssh_key_name" { default = "ken-robbike1-TL-NX-SLT" }
#variable "ssh_key_name" { default = "YOUR_SSH_KEY_HERE" }

# These SGs work, but the SGs are not really defined properly.
variable "webtier_elb_sgs" { default = ["sg-e674fd9d", "sg-7074fd0b"] }
                # sg-e674fd9d = "WebDMA". Gist of SG rules follows:
                #    inbound: allow tcp/80, tcp/443 from anywhere
                #    outbound: allow all
                # sg-7074fd0b  = "default". Gist of SG rules follows:
                #    inbound: Allow SSH, RDP, ICMP from anywhere. Allow anything from within VPC
                #    outbound: Allow various including DNS, ICMP, SMTP, tcp/80, tcp/443. Allow to anything in VPC
variable "webtier_ec2_sgs" { default = ["sg-7074fd0b", "sg-db74fda0"] }
                # sg-7074fd0b  = "default". Gist of SG rules follows:
                #    inbound: Allow SSH, RDP, ICMP from anywhere. Allow anything from within VPC
                #    outbound: Allow various including DNS, ICMP, SMTP, tcp/80, tcp/443. Allow to anything in VPC
                # sg-db74fda0  = "AppVPC". Gist of SG rules follows:
                #    inbound: allow tcp/8080, tcp/8443 from within the VPC
                #             (actually as implemented looks like it accepts from any IP)
                #    outbound: allow everything (?)
                # TODO: Current SGs seem to be improperly defined. Update this when they are fixed.
