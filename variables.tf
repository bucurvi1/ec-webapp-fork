############################################################
# Review and change these values for your particular needs
############################################################

##########
# Common #
##########

variable "appname" { default = "webappbp" }

# Tags values
variable "nibr_ids"            { default = "appname=webappbp" }
variable "nibr_technicalowner" { default = "ken.robbins@novartis.com" }
variable "nibr_managedby"      { default = "terraform=webapp_blueprint" }
variable "nibr_environment"    { default = "dev" }

# Where to deploy
# For account: TL-NX-SLT
# Subnet list corresponds to zone list which corresponds to this CIDR list
# CIDRs = [10.188.250.0/27, 10.188.250.32/27]
variable "region"  { default = "us-east-1" }  # Use eu-central-1 for the Frankfurt region
variable "subnets" { default = ["subnet-7c4c0f56", "subnet-72dae904"] }
variable "zones"   { default = ["us-east-1a", "us-east-1b"] }
